#!/bin/sh

set -eu

blocklists="$1"

whitelist() {
	grep -E -v 'example\.com|example\.org'
}

listpath="/tmp/adblock.conf"
tmpfile=$(mktemp)

echo "$blocklists" |  xargs -n 1 wget -qO -  > "$tmpfile"
tr -cd '[:alnum:][:blank:]%#.~\n_-' < "$tmpfile" | nice awk '{sub(/^127\.0\.0\.1/,"0.0.0.0")} BEGIN { OFS = "" } NF == 2 && $1 == "0.0.0.0" { print "local-zone: \"", $2, "\" always_nxdomain"}' | tr '[:upper:]' '[:lower:]' | whitelist | sort -u > "$listpath" 
