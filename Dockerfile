FROM debian:stable-slim AS adblock

ENV ADLISTS "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts https://mirror1.malwaredomains.com/files/justdomains"

RUN apt update && apt -y install --no-install-recommends ca-certificates wget
COPY unbound-adblock.sh .
RUN bash unbound-adblock.sh "${ADLISTS}"

FROM debian:stable-slim

RUN apt update \
    && apt -y install --no-install-recommends unbound ca-certificates \
    && apt -y clean \
    && rm -f /etc/unbound/unbound.conf.d/* \
    && touch /etc/unbound/local.conf


COPY --from=adblock /tmp/adblock.conf /etc/unbound/adblock.conf
COPY config /etc/unbound/unbound.conf.d/base.conf
RUN /usr/lib/unbound/package-helper root_trust_anchor_update && unbound-checkconf

EXPOSE 5353/udp
EXPOSE 5353/tcp
USER unbound
ENTRYPOINT ["unbound"]
CMD ["-dvv"]
